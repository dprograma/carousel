<!DOCTYPE HTML>
<html lang ="en">
<head>
<meta charset="utf-8"  />
<meta http-equiv="X-UA-compatible" content="IE-edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title> Golden Tulip </title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>
<header>
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				<img class="sitelogo" src="img/logo.png"> 
			</a>
		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home</a></li>
				<li><a href="#">Mission</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" 
						data-toggle="dropdown">Reaching Out<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#"></a></li>
						<li><a href="#">EJB</a></li>
						<li><a href="#">Jasper Report</a></li>
						<li class="divider"></li>
						<li><a href="#">Seperated Link</a></li>
						<li class="divider"></li>
						<li><a href="#">One More Seperated Link</a></li>
					</ul>
				</li>
			</ul>
		<div class="col-md-4 col-md-offset-2 social-icons">	
			<ul class="social">
				<li><a href="#"><img src="img/social/facebook.png"></a></li>
				<li><a href="#"><img src="img/social/twitter.png"></a></li>
				<li><a href="#"><img src="img/social/googleplus.png"></a></li>
				<li><a href="#"><img src="img/social/youtube.png"></a></li>
			</ul>
			
		</div>
        <div id="rightpos" class="col-md-4 col-md-offset-1">
            <form action="#" class="search-form" role="search">
                <div class="form-group has-feedback">
            		<label for="search" class="sr-only">Search</label>
            		<input type="text" class="form-control" name="search" id="search" placeholder="search">
              		<span class="glyphicon glyphicon-search form-control-feedback"></span>
            	</div>
            </form>
        </div>
		</div>
	</nav>
</header>
<br><br><br>
	<div class="container" >
		<div class="row">
			<div id="theCarousel" class="carousel slide" data-ride="carousel" data-interval="2000" data-wrap="true">

				<ol class="carousel-indicators">
					<li data-target="#theCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#theCarousel" data-slide-to="1"></li>
					<li data-target="#theCarousel" data-slide-to="2"></li>
		
				</ol>	

				<div class="carousel-inner">
					<div class="item active">
						<img class="img-responsive" src="img/slider1.png">
						<div class="carousel-caption">
							<h1>Amazing Backgrounds</h1>
							<p>Thousands of Backgrounds for free</p>
							<p><a href="#" class="btn btn-primary btn sm">Get them now</a></p>
						</div>
					</div>

					<div class="item">
						<img class="img-responsive" src="img/slider2.png">
						<div class="carousel-caption">
							<h1>Thousands of colours</h1>
							<p>Every Colour you can imagine</p>
						</div>
					</div>

					<div class="item">
						<img class="img-responsive" src="img/slider3.png">
						<div class="carousel-caption">
							<h1>Amazing Illustrations</h1>
							<p>And they are all free</p>
						</div>
					</div>
				</div>
				<a href="#theCarousel" class="carousel-control left" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a href="#theCarousel" class="carousel-control right" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<h3>NaijaSoft Laptops (Extra Deal)</h3>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<br>	
						<h5>HP Laptops</h5><br>
							<a href="#">
								<img class="img-mod" src="img/hp.jpg">
							</a>
								<p class="text-info">Best Quality</p>
								<p class="text-justify text-custom">
									<small>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-empty"></span>
									</small>
								</p>
								<p class="text-muted text-justify">
									<small>Specification: <br>4gig RAM. i-core processor.<br> 500 gig 	harddrive
									</small>
								</p>
								<p class="text-danger text-justify">
									<small>Click to see more Info!&nbsp;
									</small>
										<button type="submit" 	role="button" class="button-info button-info:hover">
											<span class="glyphicon glyphicon-info-sign">
											</span>
										</button>
								</p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<br>	
						<h5>Dell Laptops</h5><br>
							<a href="#">
								<img class="img-mod" src="img/dell.jpg">
							</a>
								<p class="text-info">Best Quality</p>
								<p class="text-justify text-custom">
									<small>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-empty"></span>
									</small>
								</p>
								<p class="text-muted text-justify">
									<small>Specification: <br>4gig RAM. i-core processor.<br> 500 gig 	harddrive
									</small>
								</p>
								<p class="text-danger text-justify">
									<small>Click to see more Info!&nbsp;
									</small>
										<button type="submit" 	role="button" class="button-info button-info:hover">
											<span class="glyphicon glyphicon-info-sign">
											</span>
										</button>
								</p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="marquee-slide">
						<marquee>
							<a href="#"><img src="img/show1.jpg" image-caption="Great Websites"></a>
							<a href="#"><img src="img/show2.jpg" image-caption="Great Mobile Apps"></a>
							<a href="#"><img src="img/show3.jpg" image-caption="Great Web Services"></a>
							<a href="#"><img src="img/show4.jpg" image-caption="Great Web Hosting"></a>
						</marquee>
					</div>
					<div>
						<table class="table-hover table-condensed table-striped table-bordered">
							<thead class="bg-primary">
								<tr class="small">
									<th>no.</th>
									<th>brand</th>
									<th>Model</th>
									<th>screen size</th>
									<th>HDD</th>
									<th>Memory Specs</th>
									<th>Waranty</th>
								</tr>
							</thead>
							<tbody>
								<tr class="small text-success">
									<td>1</td>
									<td>Toshiba</td>
									<td>Mod2Si</td>
									<td>15"</td>
									<td>500 G/byte</td>
									<td>4 G/byte icore</td>
									<td>1 year</td>
								</tr>
								<tr class="small text-success">
									<td>2</td>
									<td>HP</td>
									<td>Mod24j</td>
									<td>15"</td>
									<td>500 G/byte</td>
									<td>4 G/byte icore</td>
									<td>1 year</td>
								</tr>
								<tr class="small text-success">
									<td>3</td>
									<td>Dell</td>
									<td>ModU98</td>
									<td>15"</td>
									<td>500 Gigabyte</td>
									<td>4 G/byte icore</td>
									<td>1 year</td>
								</tr>
								<tr class="small text-success">
									<td>4</td>
									<td>Lenovo</td>
									<td>Mod3hr</td>
									<td>15"</td>
									<td>500 G/byte</td>
									<td>4 G/byte icore</td>
									<td>1 year</td>
								</tr>
								<tr>
									<td colspan="7" class="small text-warning">There are lots more we offer. You can<br>
										Get a whole more for just less...
										<a href="#">
											<span class="text-warning">&rsaquo;&rsaquo;&rsaquo;
											</span>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
		</div>
		<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hv">
					<br>	
						<h5>Toshiba Laptops</h5><br>
							<a href="#">
								<img class="img-mod" src="img/ts.jpg">
							</a>
								<p class="text-info">Best Quality</p>
								<p class="text-justify text-custom">
									<small>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-empty"></span>
									</small>
								</p>
								<p class="text-muted text-justify">
									<small>Specification: <br>4gig RAM. i-core processor.<br> 500 gig 	harddrive
									</small>
								</p>
								<p class="text-danger text-justify">
									<small>Click to see more Info!&nbsp;
									</small>
										<button type="submit" 	role="button" class="button-info button-info:hover">
											<span class="glyphicon glyphicon-info-sign">
											</span>
										</button>
								</p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hv">
					<br>	
						<h5>Fujitsu Siemens Laptops</h5><br>
							<a href="#">
								<img class="img-mod" src="img/fj.jpg">
							</a>
								<p class="text-info">Best Quality</p>
								<p class="text-justify text-custom">
									<small>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-star"></span>
										<span class="glyphicon glyphicon-empty"></span>
									</small>
								</p>
								<p class="text-muted text-justify">
									<small>Specification: <br>4gig RAM. i-core processor.<br> 500 gig 	harddrive
									</small>
								</p>
								<p class="text-danger text-justify">
									<small>Click to see more Info!&nbsp;
									</small>
										<button type="submit" 	role="button" class="button-info button-info:hover">
											<span class="glyphicon glyphicon-info-sign">
											</span>
										</button>
								</p>
				</div>	
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<embed src="media/testing2.mp4" width="100%" height="250" scale="aspect" controller="true"></embed>
				</div>
				<br><br>
				<table class="table-bordered table-condensed table-hover">
					<thead>
						<th colspan="2">Lorem Ipsum</th>
					</thead>
					<tbody>
						<tr>
							<td><img src="img/thumb1.jpg" class="img-thumbnail"></td>
							<td><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
								</small>
							</td>
						</tr>
						<tr>
							<td><img src="img/thumb2.jpg" class="img-thumbnail"></td>
							<td><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
								tempor incididunt ut labore et dolore magna aliqua.
								</small>
							</td>
						</tr>
					</tbody>
				</table>
		</div>
	</div>
	<br>
<footer class="footerstyle">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<br>
				<ul class="footerlinks">
					<li><a href="#">Contact Us</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Terms of Agreement</a></li>
					<li><a href="#">Feedback</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<br>
				<ul class="footerlinks">
					<li><a href="#">Careers</a></li>
					<li><a href="#">Visit our Patners Forum</a></li>
					<li><a href="#">Home and Abroad</a></li>
					<li><a href="#">Why partner with us?</a></li>
				</ul>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<br><br>
				<a href="#"><img class="footerlogo" src="img/logo.png" class="img-thumbnail"></a>
			</div>
		</div>
	</div>
</footer>
<footer class="secondfooter">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<span class="text-danger h6"><?php echo "CopyRight &copy;&nbsp;" . date('Y') . " | All Rights Resevered"; ?></span>
	</div>
</footer>
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>